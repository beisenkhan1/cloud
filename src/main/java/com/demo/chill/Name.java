package com.demo.chill;

import javax.persistence.*;

@Entity
@Table(name="springnames")
public class Name {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="user_names")
    private String name;

    public Name() {
    }

    public Name(String name) {
        this.name = name;
    }

    public Name(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}