package com.demo.chill.Repos;

import org.springframework.data.jpa.repository.JpaRepository;
import com.demo.chill.Name;

import java.util.List;

public interface UserRepository extends JpaRepository<Name, Long> {
    List<Name> findAll();
}
