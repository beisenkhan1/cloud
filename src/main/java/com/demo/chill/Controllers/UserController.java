package com.demo.chill.Controllers;

import com.demo.chill.Name;
import com.demo.chill.Repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping
    public String helloWorld() {
        return "Alemge Salem";
    }

    @RequestMapping("/goodbye")
    public String goodbyeWorld() {
        return "Goodbye Alem";
    }

    @GetMapping("/add-name")
    public String addName(@RequestParam(value = "name", defaultValue = "Beigut") String name) {
        Name n = new Name(name);
        userRepository.save(n);

        return String.format("%s succesfully added", name);
    }

    @GetMapping("/get-names")
    public @ResponseBody Iterable<Name>
    getNames() {
        return userRepository.findAll();
    }
}
