package com.demo.chill;

import ch.qos.logback.core.joran.spi.ElementPath;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SpringBootApplication
@RestController
public class ChillApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChillApplication.class, args);
    }

}
